"use sctrict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
      }

      get name() {
        return this._name;
      }
      
      set name(value) {
        return this._name = value;
      }

    
      get age() {
        return this._age;
      }  

      set age(value) {
        return this._age = value;
      }

      get salary() {
          return this._salary;
      }

      set salary(value) {
        return this._salary = value;
    }
      };

let user = new Employee("Vlad", 32, 3200)
console.log (user)
 
let phrase = "Hello";

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary);
        this.lang = lang;
    }
  
    get salary() {
        return this._salary;
    }

    set salary(value) {
        return this._salary = value * 3;
    }



}

let programmer = new Programmer("Lena", 23, 3200, "ua");
console.group('Programmer: ', programmer);

let programmer2 = new Programmer("Sasha", 42, 5400, "ua");
let programmer3 = new Programmer("Vasil", 25, 3500, "ua");

console.log (programmer2);
console.log (programmer3.salary);
console.groupEnd(programmer)