import React, { Component } from "react";
import "./Modal.scss";

class Modal extends Component {
  render() {
    const { header, closeButton, text, actions, onClickHandler } = this.props;

    return (
      <div className="modalContainer" onClick={onClickHandler}>
        <div
          className="modalContent"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <header>
            <div>
              {header}
              {closeButton && (
                <button className="modalCloseBtn" onClick={onClickHandler}>
                  Х
                </button>
              )}
            </div>
          </header>
          <div>
            <p className="modalText">{text}</p>
            <div className="modalServiceButtons">{actions}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
