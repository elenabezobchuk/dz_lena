import React from "react";
import  './Button.scss';

class Button extends React.Component {

    render() {
        const { backgroundColor, text, onClickHandler } = this.props;
        return (
        <button style={{backgroundColor: backgroundColor}} onClick={ onClickHandler }type ="button">{text} </button>)

    }
}

export default Button;