import React from "react";
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";
import "./App.scss";

class App extends React.Component {
  state = {
    firstModalIsOpened: false,
    secondModalIsOpened: false,
  };
  firstOpen = () => {
    this.setState({ firstModalIsOpened: true });
    this.setState({ secondModalIsOpened: false });
  };
  secondOpen = () => {
    this.setState({ secondModalIsOpened: true });
    this.setState({ firstModalIsOpened: false });
  };
  firstClose = () => {
    this.setState({ firstModalIsOpened: false });
  };
  secondClose = () => {
    this.setState({ secondModalIsOpened: false });
  };

  render() {
    const { firstModalIsOpened, secondModalIsOpened } = this.state;

    return (
      <div className="App_wrapper">
        <section className="section-container">
          <Button
            text={"Open first modal"}
            backgroundColor={"green"}
            onClickHandler={this.firstOpen}
          />
          <Button
            text={"Open second modal"}
            backgroundColor={"orange"}
            onClickHandler={this.secondOpen}
          />

          {firstModalIsOpened && (
            <Modal
              header={"First modal"}
              closeButton={true}
              text={
                "Once you delete this file, it won't be possible to undo this actions."
              }
              actions={
                <div className="button-wrapper">
                  <Button
                    text={"Submit"}
                    backgroundColor={"blue"}
                    onClickHandler={this.firstClose}
                  />
                </div>
              }
              onClickHandler={this.firstClose}
            />
          )}
          {secondModalIsOpened && (
            <Modal
              header={"Second modal"}
              text={
                "Once you delete this file, it won't be possible to undo this actions."
              }
              actions={
                <div className="button-wrapper">
                  <Button
                    text={"OK"}
                    backgroundColor={"blue"}
                    onClickHandler={this.secondClose}
                  />
                  <Button
                    text={"Cancel"}
                    backgroundColor={"blue"}
                    onClickHandler={this.secondClose}
                  />
                </div>
              }
              closeButton={true}
              onClickHandler={this.secondClose}
            />
          )}
        </section>
      </div>
    );
  }
}

export default App;
