"use strict";

+function () {

    document.querySelector('.tabs-title').classList.add('active');
    document.querySelector('.tabs-content-item').classList.add('active');
    
    function selectСontent (e) {
        const target = e.target.dataset.target;
    
        document.querySelectorAll('.tabs-title, .tabs-content-item').forEach(el => el.classList.remove('active'));
        e.target.classList.add('active');
        document.querySelector('.' + target).classList.add('active');
    }
    
    document.querySelectorAll('.tabs-title').forEach(el => {
        el.addEventListener('click', selectСontent);
    });
    
    }();
