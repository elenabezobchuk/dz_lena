import gulp from 'gulp';
import browserSync from "browser-sync";
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import concat from 'gulp-concat';
import terser from 'gulp-terser';
import autoprefixer from 'gulp-autoprefixer';
import cleancss from "gulp-clean-css";
import imagemin from 'gulp-imagemin';
import clean from 'gulp-clean';


const sass = gulpSass(dartSass);
const BS = browserSync.create();

const buildStyles = () => gulp.src("./src/styles/**/*.scss")
   .pipe(sass())
   .pipe(concat("styles.min.css"))
   .pipe(autoprefixer({
      cascade: false
   }))
   .pipe(cleancss())
   .pipe(gulp.dest("./dist/css"))

const buildJs = () => gulp.src("./src/ js/script.js")
   .pipe(concat("scripts.min.js"))
   .pipe(terser())
   .pipe(gulp.dest("./dist/js"))

const minImages = () => gulp.src("./src/img/**/*")
   .pipe(imagemin())
   .pipe(gulp.dest("./dist/img"))

const cleanDest = () => gulp.src('./dist/*',{read: false})
   .pipe(clean())

export const build = gulp.series(cleanDest, gulp.parallel(minImages, buildStyles, buildJs,));

export const dev = gulp.series(build, () => {
    browserSync.init({
       server: {
          baseDir: "./"
       }
    })
 
    gulp.watch('./src/**/*', gulp.series(build, (done) => {
       BS.reload();
       done()
    }))
 });
 



