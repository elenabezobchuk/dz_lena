Чтоб не плодить код и его дубли, делать его более структурированным и компактным и нужны функции. Функция - это как подпрограмма внутри основной программы, блок кода, который по имени можно вызвать из основной программы, причем неоднократно.

Они упрощают и сокращают код, адаптируют его для разных платформ, делают его более отказоустойчивым, легко отлаживать.

Функция, как часть основной программы, взаимодействует с ней через аргументы - конкретные значения, которые передаются параметрам при вызове функции. Затем функция их обрабатывает и выдает-возвращает результат - какое-то значение.