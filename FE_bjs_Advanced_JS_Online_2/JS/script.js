'use strict';

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

    try {
        function booksListMaker (...args){
            const booksList = document.querySelector("#root");
            if(books.hasOwnProperty(author) && books.hasOwnProperty(name) && books.hasOwnProperty(price)) {
                booksList.innerHTML = books.map(e => `<li>${Object.values(e).join(': ')}</li>`).join(' ');
                return booksList;
            }
        }
            booksListMaker();
        
    } catch (error) {
        console.warn(`${error.name}${':'} ${error.message}`);
        
    } 
    finally {
        const booksNewArr = [];
        books.forEach((book) => {
        if (Object.keys(book).length === 3){
          booksNewArr.push(book);
        } 
    })
        function booksListMaker (...args){
        const booksList = document.querySelector("#root");
        booksList.innerHTML = booksNewArr.map(e => `<li>${Object.values(e).join(': ')}</li>`).join(' ');
        return booksList;
        }
        booksListMaker();
    }
  






