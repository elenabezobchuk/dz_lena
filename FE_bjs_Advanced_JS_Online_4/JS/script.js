'use strict';
const url = 'https://ajax.test-danit.com/api/swapi/films';
const ul = document.querySelector("ul");  

async function fetchFilmsList() {
   const response = await fetch(url);
   const dataToJson = await response.json();
   for (let element of dataToJson) {
      addFilmName(element, ul);
      element.characters.forEach((characters) => {
         fetchFilmsCharacters(characters, element);
      });
   }
};

fetchFilmsList();

function addFilmName({name, episodeId, openingCrawl }, parentElement) {
   const li = document.createElement("li");
   li.dataset.id = episodeId;
   li.style = `list-style-type: none`;
   li.insertAdjacentHTML('beforeend', `<h2>Film: ${name}</h2><h4>Episode: ${episodeId}</h><p>Description: ${openingCrawl}</p><h3>Characters name:</h3><ul></ul>`);
   parentElement.append(li);
};

async function fetchFilmsCharacters(characters, data) {
   const response = await fetch(characters);
   const dataToJson = await response.json();
   addFilmsCharacters(dataToJson, data.episodeId);
};

function addFilmsCharacters({ name }, id) {
   let rootElement = document.querySelector(`[data-id="${id}"]`);
   let ul = rootElement.querySelector("ul");
   let li = document.createElement("li");
   li.style = `list-style-type: none`;
   li.textContent= name;
   ul.append(li);
};
   
