Що таке AJAX і чим він корисний при розробці Javascript?

Це синтез технологій JavaScript та XML, основний сенс якого - асинхронність (неодночасність) роботи веб-інтерфейсу: під час однієї користувацької сесії у фоновому режимі (непомітно для користувача, без перезавантаження сторінки) відбувається взаємодія з віддаленим сервером та відправка/отримання необхідних користувачу даних.
Використання AJAX сприяє створенню швидких та ефективніших веб-додатків.
При розробці JS він корисний тим, що є своєрідним місточком між веб-сервером та веб-браузером і дозволяє динамічно вирішувати чимало практичних і найбільш затребуваних користувачами завдань: 
- Опрацьовувати облікові дані для входу в систему в формі на сторінці та оновлення сторінки;
- Автозавершення сеансу на пошукових сервісах типу Google
- В тому ж Google завдяки AJAX користувач в режиму реального часу може маніпулювати даними та змінювати налаштування перегляду;
- Оновлення вмісту користувача тощо.

