'use strict';
const btn = document.querySelector("button");
const ul = document.createElement("ul");
const ipUrl = "https://api.ipify.org/?format=json";
const userInfoUrl = "http://ip-api.com/";

btn.addEventListener("click", getIPUser);

async function getCastomInfo(url, param = "") {
   const response = await fetch(`${url}${param}`);
   const dataToJson = await response.json();
   return dataToJson
}

async function getIPUser() {
   const dataIP = await getCastomInfo(ipUrl);
   const dataInfo = await getCastomInfo(userInfoUrl, `json/${dataIP.ip}?fields=country,regionName,city,district,continent`);
   for (let el in dataInfo) {
      addCastomInfo(el, dataInfo[el]);
   }
   document.body.append(ul);
}

async function addCastomInfo(el, info) {
   const li = document.createElement("li");
   li.innerHTML = `${el}: ${info}`;
   ul.append(li);
}
