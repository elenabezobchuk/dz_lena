"use strict";

class Card {
    constructor(name, email, title, text, postId) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.postId = postId;
        this.divCard = document.createElement('div');
        this.btnDelete = document.createElement('button');
    }

    createElements() {
        this.divCard.className = 'card';
        this.divCard.innerHTML = `<h2>${this.name}</h2>
                                <a href="mailto:${this.email}">${this.email}</a>
                                <h3>${this.title}</h4>
                                <p>${this.text}</p>`
        this.btnDelete.className = 'card__btn';
        this.btnDelete.insertAdjacentHTML('beforeend', '<p>X</p>');
        this.divCard.append(this.btnDelete);
        this.btnDelete.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
                method: 'DELETE',
            })
                .then(({status}) => {
                    if (status === 200) {
                        this.divCard.remove()
                    } else {
                        throw new Error();
                    }
                })
                .catch((err) => console.warn(err.message));
        })
    }

    render(selector) {
        this.createElements();
        document.querySelector(selector).prepend(this.divCard);
    }
}

fetch('https://ajax.test-danit.com/api/json/users')
    .then((response) => {
        return response.json();
    }
    )
    .then((res) => {
        res.forEach(({id: userId, name, email}) => {
            fetch(`https://ajax.test-danit.com/api/json/users/${userId}/posts`)
                .then((response) => {
                    return response.json();
                })
                .then((res) => {
                    res.forEach(({title, body, id: postID}) => {
                        const cardInstance = new Card(name, email, title, body, postID);
                        cardInstance.render(".container");
                        })
                    })
            })
        }
    )
