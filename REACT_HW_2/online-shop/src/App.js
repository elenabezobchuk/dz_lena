import {PureComponent} from 'react';
import Header from './Components/Header/Header';
import ProductList from './Components/ProductList/ProductList';
import Modal from './Components/Modal/Modal';
import styles from './App.module.scss';

class App extends PureComponent {
    
    state = {
        products: [],
        cart: [],
        favorites: [],
        isOpenModal: false,
        modalProps: {},
      }
    
      addToCart = (card) => {
        this.setState((current) => {
          const newCart = [...current.cart, card]
          localStorage.setItem("cart", JSON.stringify(newCart))
          return {cart:newCart}
        })
      }
    
      addToFavorites = (id) => {
        this.setState((current) => {
          const favoritesId = [...current.favorites, id]
          localStorage.setItem("favorites", JSON.stringify(favoritesId))

    
          const products = [...current.products]
          const index = products.findIndex(el => el.id === id)
          products[index].isFavorite = true
          return {favorites: favoritesId, products}
      })
      }
    
      setIsOpenModal = (value) => {
        this.setState({ isOpenModal: value })
      }
    
      setModalProps = (obj) => {
        this.setState({modalProps : obj})
        console.log(this.state.modalProps);
    
      }
    
    async componentDidMount() {
        
        const products = await fetch("./data.json").then(res => res.json())
        this.setState({products})

        const cart = localStorage.getItem("cart")
        if (cart) {this.setState({cart : JSON.parse(cart)})}
    
        const favoritesStorage = localStorage.getItem("favorites") 
        if (favoritesStorage) {
          const favorites = JSON.parse(favoritesStorage)
          this.setState((current) => {
            const products = [...current.products]
            products.forEach(product => {
              if (favorites.indexOf(product.id) >= 0) {
                product.isFavorite = true
              }
            })
    
            return {products, favorites}
          })
        }
      }
    
    
    render() {
        const {products, cart, favorites, isOpenModal, modalProps} = this.state;

    return (
        <div className={styles.App}>
            <Header cartCount={cart.length} favoriteCount={favorites.length} />
            <ProductList 
        cards = {products} 
        addToCart={this.addToCart} 
        addToFavorites={this.addToFavorites}
        setIsOpenModal={this.setIsOpenModal}
        setModalProps={this.setModalProps}/>

        {isOpenModal && <Modal
        modalProps={modalProps}
        setIsOpenModal={this.setIsOpenModal} />}

                </div>
    )
}}



export default App;
