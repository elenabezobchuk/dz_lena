import {PureComponent} from 'react';
import styles from './Header.module.scss';
import shoppingCartIcon from './cart-icon.svg';
import favoriteIcon from '../../svg/isFavoriteStar.svg';
import PropTypes from "prop-types"


class Header extends PureComponent {

    render() {
        const { cartCount, favoriteCount } = this.props;
    
        return (
            <header className={styles.root}>
                <nav className={styles.navContainer}>
                    <div className={styles.container}>
                        <div className={styles.logoWrap}>
                            <a href="#"><img className={styles.logo} width={50} height={50} alt = ""/></a>
                            <p className={styles.logoText}>WineHouse</p>
                        </div>
                            <ul>
                                <li>
                                     <a href="">Про магазин</a>
                                </li>
                                <li>
                                    <a href="">Наша продукція</a>
                                </li>
                                <li>
                                    <a href="">Блог</a>
                                </li>
                            </ul>
                            <div className={styles.iconContainer}>
                        <div className={styles.icon}>
                            <img src={favoriteIcon} alt="" />
                            {(favoriteCount > 0) && <p className={styles.favoriteCountText}>{favoriteCount}</p>}
                            
                        </div>

                        <div className={styles.icon}>
                            <img src={shoppingCartIcon} alt="" />
                            {(cartCount > 0) && <p>{cartCount}</p>}
                            
                        </div>
                            
                    </div>
                    
                    </div>

                </nav>
            </header>
        );
    }
}
Header.propTypes = {
    cartCount: PropTypes.number,
    favoriteCount: PropTypes.number
}

Header.defaultProps = {
    cartCount: 0,
    favoriteCount: 0
}
  
export default Header;