import { PureComponent } from "react";
import styles from './ProductCard.module.scss';
import starFilled from '../../svg/isFavoriteStar.svg';
import starOutline from '../../svg/isntFavoriteStar.svg';
import Button from '../Button/Button';
import PropTypes from "prop-types";


class ProductCard extends PureComponent {

    render () {
        const {addToCart, title, img, volume, isFavorite, id, price, addToFavorites, setIsOpenModal, setModalProps} = this.props;
        return (
            <div className={styles.card}>
               <button type="button" className={styles.favoriteButton} onClick={() => {addToFavorites(id)}}>
                    <img className={styles.favoriteIcon} src={isFavorite ? starOutline : starFilled } alt="" />
                </button>
            <span className={styles.title}>{title}</span>
            <img className={styles.img} src={img}
                 alt={title}/>
            <span className={styles.volume}>{volume}</span>
            <span className={styles.price}>{price}</span>
            <div className={styles.btnContainer}>
                <Button text="Додати до кошика" 
                backgroundColor="rgb(201, 14, 14)" 
                onClick={() => {
                    setModalProps({
                        header: "Додати до кошика", 
                        text: `Ви дійсно хочете додати до кошика ${title}?`, 
                        onSubmit: ()=>{
                            addToCart({title, img, id, price, isFavorite})
                            setIsOpenModal(false);
                        }
                    });
                    setIsOpenModal(true);
                }}/>

            </div>
        </div>
        ) 
    
    }

        
    }

    ProductCard.propTypes = {
        title: PropTypes.string.isRequired, 
        img: PropTypes.string.isRequired, 
        id: PropTypes.string.isRequired, 
        price: PropTypes.number.isRequired, 
        volume: PropTypes.number.isRequired,
        isFavorite: PropTypes.bool, 
        addToCart: PropTypes.func, 
        addToFavorites: PropTypes.func, 
        setIsOpenModal: PropTypes.func, 
        setModalProps: PropTypes.func
        
    }
   
    ProductCard.defaultProps = {
        isFavorite: false, 
        addToCart: () => {}, 
        addToFavorites: () => {}, 
        setIsOpenModal: () => {}, 
        setModalProps: () => {}
    }
    
export default ProductCard