import React, { PureComponent } from 'react';
import ProductCart from '../ProductCard/ProductCard';
import styles from './ProductList.module.scss';
import PropTypes from "prop-types";

class ProductList extends PureComponent {

    render (){
        const { addToCart, cards, addToFavorites, setIsOpenModal, setModalProps } = this.props;

        return (
            <div>
				<ul className={styles.list}>
					 {cards.map(({title, img, volume, price, isFavorite, id}) => (
						<li key={id}>
							<ProductCart
								addToCart={addToCart}
								id={id}
								title={title}
								volume={volume}
								img={img}
                                price={price}
								isFavorite={isFavorite}
                    			addToFavorites={addToFavorites}
                    			setModalProps={setModalProps}
                    			setIsOpenModal={setIsOpenModal}

								/>
						</li>
					 ))}
				</ul>
			</div>

        )
    }
}
ProductList.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.object),
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    setIsOpenModal: PropTypes.func,
    setModalProps: PropTypes.func
}

ProductList.defaultProps = {
    cards: [],
    addToCart: ()=>{},
    addToFavorites: () => {},
    setIsOpenModal: () => {},
    setModalProps: () => {}
}

export default ProductList