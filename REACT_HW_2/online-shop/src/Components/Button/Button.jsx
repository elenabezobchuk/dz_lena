import { PureComponent } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends PureComponent {
    render (){
        const {backgroundColor, text, onClick, type} = this.props;

        return (
            <button className={styles.btn} style={{backgroundColor}} onClick={onClick} type={type}> {text}</button>


        )
    }
}
Button.propTypes = {
    onClick : PropTypes.func.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(["button", "submit", "reset"]),
}

Button.defaultProps = {
    backgroundColor: "rgb(201, 14, 14)",
    text: "Button",
    type: "button",
}

export default Button
