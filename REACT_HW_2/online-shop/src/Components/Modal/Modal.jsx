import {PureComponent} from 'react';
import styles from './Modal.module.scss';
import PropTypes from "prop-types";
import Button from '../Button/Button';

class Modal extends PureComponent {
    render() {
      const { modalProps:{header, text, onSubmit}, setIsOpenModal } = this.props
        

        return ( 
        <div className={styles.backgroundContainer} onClick = {()=>{setIsOpenModal(false)}}>
                <div className={styles.contentContainer} onClick = {event => event.stopPropagation()}>
                    <div className={styles.header}>
                        <p>{header}</p>
                        <button className={styles.closeButton} onClick = {()=>{setIsOpenModal(false)}}></button>
                    </div>
                    <div className={styles.body}>
                        <p>{text}</p>
                    </div>
                    <div className={styles.footer}>
                        <Button 
                        text="Так" 
                        backgroundColor="rgb(201, 14, 14)" 
                        onClick={onSubmit}/>
                        <Button 
                        text="Ні" 
                        backgroundColor="grey" 
                        onClick={()=>{setIsOpenModal(false)}}/>
                    </div>
                </div>
            </div>)
    }
}

Modal.propTypes = {
    modalPrors: PropTypes.shape({
        header: PropTypes.string,
        text: PropTypes.string,
        onSubmit: PropTypes.func
    }),
    setIsOpenModal: PropTypes.func
}

Modal.defaultProps = {
    modalPrors : {
        header: "Modal title",
        text: "Modal text",
        onSubmit: () => {console.log("onSubmit");}
    },
    setIsOpenModal: () => {}
}

  export default Modal;