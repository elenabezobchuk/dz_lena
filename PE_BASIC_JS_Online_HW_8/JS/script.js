'use strict';
const allItems = [...document.querySelectorAll ("p")];
console.log (allItems);
allItems.forEach ((element) => {
    element.style.color = "#ff0000";
});

const item = document.getElementById ("optionsList");
console.log (item);
console.log (item.parentElement);
console.log (item.childNodes);

const p = document.getElementsByClassName ("testParagraph");
p.textContent = "This is a paragraph";
console.log (p);


const mainHeaderList = document.body.getElementsByClassName ('main-header');
for (let elem of mainHeaderList) {
    console.log (elem.children);
    console.log (elem.firstElementChild.className);
    console.log (elem.lastElementChild.className);
    console.log (elem.firstElementChild.className.replace ("container main-header-content", "nav-item"));
    console.log (elem.lastElementChild.className.replace ("container d-flex", "nav-item"));
     
};

const removeSelector = document.querySelectorAll(".options-list-title");
// console.log(removeSelector);
removeSelector.forEach((selector) => {
   selector.classList.add(".section-title");
   selector.classList.remove(".section-title");
    
});


